# importing library
library(reshape2) # to change the format; wide format -> long format

active = read.csv('active.csv') # read the csv file 'active.csv'

head(active)

# converting to long format using 'melt' from reshape 2
active_long <- melt(active,
                    id.vars=c("id", "sex"), # 'id.vars' provides a vector of variables that will be repeated (not stacked) in the new long format data set
                    measure.vars=c("hvltt", "hvltt2", "hvltt3", "hvltt4"), # 'measure.vars' tells the variables in the wide-format to be stacked in the long format
                    variable.name="time", # 'variable.name' -> column name for the variables 
                    value.name="hvltt" # 
                    )

head(active_long)

# one within subject factor -> here it is time; how the memory performance differ with time

# We first consider just one within-subject factor, time, to evaluate whether there is any difference in verbal ability across the 
# 4 times of data. To answer the question, the following analysis can be conducted

ex1 <- aov(hvltt ~ time + Error(id/time), data = active_long)

summary(ex1)

# To look at the effect of "time", we check the output in the "Error: Within" section. For the F value 13.12 with the degrees of 
# freedom 3 and 6292, we have p-value 1.54e-08. Therefore, there is a significant difference among the 4 times of data.

# In this case, the outcome is hvltt (verbal ability score or values) and the predictor is time (hvltt, hvltt2, hvltt3, hvltt4).
# Error(id/time) is used to divide the error variance into 4 different clusters, which therefore takes into account of the repeated measures.


# One within-subject factor (time) and one between-subject factor (sex)

# With the two factors, we can also test the interaction effect between time and sex

ex2 <- aov(hvltt ~ time*sex + Error(id / (time*sex)), data = active_long)

summary(ex2)

# we have significant time and sex effects but no interaction effect